import React, { useState } from 'react';
import { ScrollView, View, Dimensions, TextInput, Image, TouchableOpacity, Text, FlatList } from 'react-native';
import { Header, Thumbnail, Button, Fab } from "native-base"
import { SliderBox } from "react-native-image-slider-box";
import moment from "moment"
import { pull } from "lodash"

const deviceWidth = Dimensions.get("window").width
const socialMatchesData = [
  {
    matchPercentage: 6,
    name: "Nurul Ezzati monnn",
    company: "PEROLIAM NASIONAL BERHAD (sEEd Lab)"
  }, {
    matchPercentage: 6,
    name: "Nurul Ezzati monnn",
    company: "PEROLIAM NASIONAL BERHAD (sEEd Lab)"
  }, {
    matchPercentage: 6,
    name: "Nurul Ezzati monnn",
    company: "PEROLIAM NASIONAL BERHAD (sEEd Lab)"
  }, {
    matchPercentage: 6,
    name: "Nurul Ezzati monnn",
    company: "PEROLIAM NASIONAL BERHAD (sEEd Lab)"
  }, {
    matchPercentage: 6,
    name: "Nurul Ezzati monnn",
    company: "PEROLIAM NASIONAL BERHAD (sEEd Lab)"
  },
]

const postData = [
  {
    name: "Roy Ong",
    date: new Date(),
    details: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend dignissim convallis. Mauris consectetur eu ipsum eu vulputate. Donec diam metus, condimentum vitae dictum vitae, elementum sed sapien. Pellentesque mauris justo, porttitor non consectetur at, tempus vitae diam. Sed sodales lectus et dignissim lacinia. Phasellus ac metus diam. Proin non lectus dictum, tristique libero vel, ultricies metus. Sed eget felis eu risus aliquam pretium. Duis eu condimentum orci, nec porta ligula. Donec malesuada molestie nulla, non consequat eros malesuada eu",
    comments: 0,
    likes: 1
  },
  {
    name: "Roy Ong",
    date: new Date(),
    details: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend dignissim convallis. Mauris consectetur eu ipsum eu vulputate. Donec diam metus, condimentum vitae dictum vitae, elementum sed sapien. Pellentesque mauris justo, porttitor non consectetur at, tempus vitae diam. Sed sodales lectus et dignissim lacinia. Phasellus ac metus diam. Proin non lectus dictum, tristique libero vel, ultricies metus. Sed eget felis eu risus aliquam pretium. Duis eu condimentum orci, nec porta ligula. Donec malesuada molestie nulla, non consequat eros malesuada eu",
    comments: 0,
    likes: 1
  },
  {
    name: "Roy Ong",
    date: new Date(),
    details: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend dignissim convallis. Mauris consectetur eu ipsum eu vulputate. Donec diam metus, condimentum vitae dictum vitae, elementum sed sapien. Pellentesque mauris justo, porttitor non consectetur at, tempus vitae diam. Sed sodales lectus et dignissim lacinia. Phasellus ac metus diam. Proin non lectus dictum, tristique libero vel, ultricies metus. Sed eget felis eu risus aliquam pretium. Duis eu condimentum orci, nec porta ligula. Donec malesuada molestie nulla, non consequat eros malesuada eu",
    comments: 0,
    likes: 1
  },
  {
    name: "Roy Ong",
    date: new Date(),
    details: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend dignissim convallis. Mauris consectetur eu ipsum eu vulputate. Donec diam metus, condimentum vitae dictum vitae, elementum sed sapien. Pellentesque mauris justo, porttitor non consectetur at, tempus vitae diam. Sed sodales lectus et dignissim lacinia. Phasellus ac metus diam. Proin non lectus dictum, tristique libero vel, ultricies metus. Sed eget felis eu risus aliquam pretium. Duis eu condimentum orci, nec porta ligula. Donec malesuada molestie nulla, non consequat eros malesuada eu",
    comments: 0,
    likes: 1
  },
  {
    name: "Roy Ong",
    date: new Date(),
    details: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend dignissim convallis. Mauris consectetur eu ipsum eu vulputate. Donec diam metus, condimentum vitae dictum vitae, elementum sed sapien. Pellentesque mauris justo, porttitor non consectetur at, tempus vitae diam. Sed sodales lectus et dignissim lacinia. Phasellus ac metus diam. Proin non lectus dictum, tristique libero vel, ultricies metus. Sed eget felis eu risus aliquam pretium. Duis eu condimentum orci, nec porta ligula. Donec malesuada molestie nulla, non consequat eros malesuada eu",
    comments: 0,
    likes: 1
  },
  {
    name: "Roy Ong",
    date: new Date(),
    details: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend dignissim convallis. Mauris consectetur eu ipsum eu vulputate. Donec diam metus, condimentum vitae dictum vitae, elementum sed sapien. Pellentesque mauris justo, porttitor non consectetur at, tempus vitae diam. Sed sodales lectus et dignissim lacinia. Phasellus ac metus diam. Proin non lectus dictum, tristique libero vel, ultricies metus. Sed eget felis eu risus aliquam pretium. Duis eu condimentum orci, nec porta ligula. Donec malesuada molestie nulla, non consequat eros malesuada eu",
    comments: 0,
    likes: 1
  },
]

const bannerImage = [
  "https://source.unsplash.com/1024x768/?nature",
  "https://source.unsplash.com/1024x768/?water",
  "https://source.unsplash.com/1024x768/?girl",
  "https://source.unsplash.com/1024x768/?tree"
]

const App = () => {

  const [seeMore, setSeeMore] = useState([])

  const renderSocialMatches = ({ item, index }) => {
    return (
      <View style={{ width: deviceWidth / 3, height: deviceWidth / 2.5, borderColor: "#dedede", borderWidth: 2, marginRight: socialMatchesData.length - 1 === index ? 0 : 15, borderRadius: 3, alignItems: "center", padding: 10 }}>
        <Thumbnail source={require("./assets/images/ayame.jpg")} />
        <Text style={{ fontSize: deviceWidth / 38, fontWeight: "bold", color: "green" }}> {item.matchPercentage}% Match</Text>
        <Text style={{ fontSize: deviceWidth / 30, fontWeight: "bold", color: "#024238" }} numberOfLines={1}>{item.name}</Text>
        <Text style={{ fontSize: deviceWidth / 40, textAlign: "center", color: "darkgray" }} >{item.company}</Text>

        <View style={{ justifyContent: "flex-end", flex: 1, width: "100%" }}>
          <Button style={{ height: deviceWidth / 20, backgroundColor: "#024238" }} full>
            <Text style={{ fontSize: deviceWidth / 38, textAlign: "center", color: "white" }}>{"Follow"}</Text>
          </Button>
        </View>
      </View>
    )
  }

  const renderPosts = ({ item, index }) => {
    return (
      <View style={{ padding: 5, paddingVertical: 15, borderBottomColor: "#dedede", borderBottomWidth: 1, marginHorizontal: 10 }}>
        <View style={{ flexDirection: "row" }}>
          <Thumbnail small source={require("./assets/images/ayame.jpg")} />
          <View style={{ marginLeft: 10, alignContent: "center", marginTop: 2 }}>
            <Text style={{ fontSize: 14, fontWeight: "bold", color: "#024238" }} numberOfLines={1}>{item.name}</Text>
            <Text style={{ fontSize: 10, textAlign: "center", color: "darkgray" }} >{moment(item.date).format("DD MMM YYYY h:mm a")}</Text>
          </View>

        </View>
        <Text numberOfLines={seeMore.includes(index) ? 0 : 2} style={{ fontSize: deviceWidth / 40, color: "darkgray", marginTop: 10 }}>{item.details}</Text>
        <TouchableOpacity onPress={() => onChangeTextShow(index)}>
          <Text style={{ fontWeight: "bold", color: "gray", fontSize: 12, textDecorationLine: "underline", marginTop: 5 }}>{seeMore.includes(index) ? "Show less" : "See more"}</Text>
        </TouchableOpacity>

        <View style={{ flexDirection: "row", justifyContent: "space-between", marginTop: 20 }}>
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <Image source={require("./assets/images/love.png")} style={{ width: 15, height: 15 }} />
            <Text style={{ color: "#024238", marginLeft: 5, fontSize: 10 }}>{item.likes} likes</Text>
          </View>

          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <Image source={require("./assets/images/comment.png")} style={{ width: 15, height: 15 }} />
            <Text style={{ color: "#024238", marginLeft: 5, fontSize: 10 }}>{item.comments} comments</Text>
          </View>

        </View>

      </View>
    )
  }

  const onChangeTextShow = (index) => {
    if (seeMore.includes(index)) {
      pull(seeMore, index)
      setSeeMore([...seeMore])
    }
    else {
      seeMore.push(index)
      setSeeMore([...seeMore])
    }
  }



  return (
    <View style={{ flex: 1 }}>
      <Header
        style={{ backgroundColor: "#ffcb4e" }}
        androidStatusBarColor={"#9b7c28"}
      >
      </Header>
      <ScrollView style={{ flex: 1 }}>
        <View style={{ alignSelf: "center", marginVertical: 15, height: 100 }}>
          <SliderBox circleLoop autoplay={true} images={bannerImage} parentWidth={deviceWidth - 30} sliderBoxHeight={100} dotColor={"transparent"} inactiveDotColor={"transparent"} />
        </View>

        <View style={{ flexDirection: "row", paddingHorizontal: 15, maxHeight: 40, marginBottom: 15 }}>
          <Thumbnail small source={require("./assets/images/ayame.jpg")} />
          <View style={{ flex: 1, borderWidth: 1.5, marginLeft: 10, borderColor: "#dedede", flexDirection: "row", alignItems: "center" }}>
            <TextInput style={{ flex: 1, marginLeft: 10 }} placeholder={"Write a post...."} />
            <TouchableOpacity>
              <Image source={require("./assets/images/send.png")} style={{ width: 30, height: 30, marginRight: 5 }} />
            </TouchableOpacity>
          </View>
        </View>

        <View style={{ paddingHorizontal: 15 }}>
          <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
            <View>
              <Text style={{ fontSize: 18, fontWeight: "bold", color: "#014238" }}>Social Matches</Text>
              <Text style={{ fontSize: 12, color: "#014238" }}>Make quick connections.</Text>
            </View>
            <View>
              <TouchableOpacity>
                <Text style={{ fontSize: 11, color: "darkgray", fontWeight: "bold" }}>{"See All >"}</Text>
              </TouchableOpacity>
            </View>
          </View>

          <FlatList
            style={{ marginTop: 10 }}
            data={socialMatchesData}
            renderItem={renderSocialMatches}
            horizontal={true}
            showsHorizontalScrollIndicator={false}
          />
        </View>

        <FlatList
          style={{ marginTop: 10 }}
          data={postData}
          renderItem={renderPosts}
          extraData={seeMore}
        />

      </ScrollView>

      <Fab
        active={false}
        style={{ backgroundColor: '#ffcb4e' }}
        position="bottomRight"
        containerStyle={{right: '10%'}} 
        >
        <Image source={require("./assets/images/pen.png")} style={{ width: 25, height: 25 }} />
      </Fab>

    </View>
  );
};

export default App;
